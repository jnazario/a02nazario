# 44563-02 A01 -- Jordan Nazario
My personal website created to practice HTML, CSS, and JS

# Pages

1. Home page - Contains a small introduction of who I am.
2. Salary Calculator - Contains an embedded youtube video of how to calculate net salary and several inputs that allow the user to calculate their net salary based off of gross salary and effective tax rates
3. Contact page - Contains multiple links and addresses to contact me and a form that can be used to send me small messages.