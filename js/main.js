var salary;
var stateIncomeTaxRate;
var federalIncomeTaxRate;
var localIncomeTaxRate;

$(function(){
    $('input[type="radio"]').click(function(){
        if($(this).attr('id') =="hourly"){
            $("#hourlyDiv").show();
            $("#salaryDiv").hide();
        }
        else{
            $("#salaryDiv").show();
            $("#hourlyDiv").hide();
        }
    })
})

function getSalariedInfo(){
    salary = parseInt($("#salaryText").val());
    $("#grossSalary").html(salary);
    let fica =getFICADeduction(parseInt(salary));
    $("#ficaDeduction").html(Math.round(100*fica)/100);
    stateIncomeTaxRate = $("#salariedStateRate").val()/100;
    let stateDeduction = (salary*stateIncomeTaxRate);
    $("#stateIncome").html(Math.round(100*stateDeduction)/100);
    federalIncomeTaxRate = $("#salariedFederalRate").val();
    let fedDeduction = (salary*federalIncomeTaxRate)/100;
    $("#federalIncome").html(Math.round(100*fedDeduction)/100);
    localIncomeTaxRate = $("#salariedLocalRate").val();
    let localDeduction = (salary*localIncomeTaxRate)/100;
    $("#localIncome").html(Math.round(100*localDeduction)/100);
    let netSalary = Math.round(100*(salary - stateDeduction - fedDeduction - localDeduction -fica))/100;
    $("#netSalary").html(netSalary);
}

function getHourlyInfo(){
    let hours = $("#hourlyHours").val();
    let wage = $("#hourlyWage").val();
    salary = parseInt(hours*wage*52);
    $("#grossSalary").html(salary);
    let fica = getFICADeduction(salary);
    $("#ficaDeduction").html(Math.round(100*fica)/100);
    stateIncomeTaxRate = $("#hourlyStateRate").val();
    let stateDeduction = (salary*stateIncomeTaxRate)/100;
    $("#stateIncome").html(Math.round(100*stateDeduction)/100);
    federalIncomeTaxRate = $("#hourlyFederalRate").val();
    let fedDeduction = (salary*federalIncomeTaxRate)/100;
    $("#federalIncome").html(Math.round(100*fedDeduction)/100);
    localIncomeTaxRate = $("#hourlyLocalRate").val();
    let localDeduction = (salary*localIncomeTaxRate)/100;
    $("#localIncome").html(Math.round(100*localDeduction)/100);
    let netSalary = Math.round(100*(salary - stateDeduction - fedDeduction - localDeduction -fica))/100;
    $("#netSalary").html(netSalary);

}

function getFICADeduction(grossSalary){
    if(typeof grossSalary !== "number"){
        throw Error("The input salary is not a number!");
    }
    else if(grossSalary<1){
        throw Error("The input salary cannot be less than 1!");
    }
    else if(grossSalary<127000){
        let medicare = grossSalary*.0145;
        let ss = grossSalary*.062;
        let fica= ss+medicare;
        return Math.round(100*fica)/100;
    }
    else if(grossSalary<=200000){
        let ss= 127000*.062;
        let medicare = grossSalary*.0145;
        let fica= ss+medicare;
        return Math.round(100*fica)/100;
    }
    else{
        let ss= 127000*.062;
        let medicare = grossSalary*.0235;
        let fica = ss+medicare;
        return Math.round(100*fica)/100;
    }
}


